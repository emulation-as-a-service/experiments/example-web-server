#!/usr/bin/env node

const fs = require("fs");

process.on("uncaughtException", e => {
  console.log(e);
});

require("http")
  .createServer((req, res) => {
    switch (req.url) {
      case "/input":
        try {
          res.end(fs.readFileSync("/input/input.json"));
        } catch {}
        break;
      case "/output":
        try {
          req.pipe(fs.createWriteStream("/output/output.json"));
          res.end("OK");
        } catch {}
        break;
      default:
        res.end("PHANTASTIC!\n");
    }
  })
  .listen(80);
