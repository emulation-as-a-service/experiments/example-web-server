from node:11-alpine
run apk add busybox-extras
copy app.js /app.js
copy app /app
cmd /app
